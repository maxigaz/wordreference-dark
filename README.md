# WordReference Dark Theme

A dark theme for [WordReference.com].

This style is written in the [usercss] format, specifically for [Stylus], although it might be compatible with other style managers such as [xStyle] as well.

## Install with Stylus

If you have Stylus installed, click on the banner below and a new window of Stylus should open, asking you to confirm to install the style.

**Development version** (updated after every commit, reinstall it manually to see the latest changes): [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)][latest development version]

## Screenshots

Coming soon™

## Mirrors

The style can also be installed from the following websites (although these versions may be slightly out of date):

OpenUserCSS: coming soon™

## Submitting issues

Please, install the [latest development version] before reporting any issues.

In the issue description, include the following:

- An example URL to the page you’re experiencing the problem on or provide step by step instructions on how to reproduce it.
- The version of your web browser and the userstyle.
- In addition, including a screenshot or screencast of the problem is also helpful.

## Credits

The userstyle is inspired by [Arc Dark], although it doesn't follow all its design choices.

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here][List of styles on GitLab].



[WordReference.com]: https://www.wordreference.com/
[usercss]: https://github.com/openstyles/stylus/wiki/Usercss
[Stylus]: https://add0n.com/stylus.html 
[xStyle]: https://github.com/FirefoxBar/xStyle
[latest development version]: https://gitlab.com/maxigaz/wordreference-dark/raw/master/wordreference-dark.user.css
[Arc Dark]: https://github.com/horst3180/Arc-theme
[List of styles on GitLab]: https://gitlab.com/maxigaz/userstyles
[WordReference.com]: https://gitlab.com/maxigaz/gitlab-dark
